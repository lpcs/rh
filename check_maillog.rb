#!/usr/bin/env ruby
#
#	Author:	Pavel Valena
#	E-mail:	pavel.valena@sserver.cz
#
#	Description: Checks postfix/smtpd outgoing emails for "authentication" and "from" email adresses mismatch.
#		Allows exceptions.
#
#	Usage:	check_maillog.rb FILE1 [FILE2 [...]]
#
#

 reg = []
 
 reg[0] = /\s+postfix\/smtpd\[[0-9]+\]:\s+([^:]+):\s+\S+,\s+\S+,\s+sasl_username=(\S+)/
 reg[1] = ' postfix\/qmgr\[[0-9]+\]: '
 reg[2] = ': from=<(\S+)>, '
 
 allow = [
 		"?????@sserver.cz <= pavel.valena@sserver.cz",  #< Authentication email
		"?????@sserver.cz <= spravce@sserver.cz"		#< replaced with ?????
		
 	]
 	
 ARGV.each {
 	|fle|
 	
	File.open(fle) {
 		|f|
 	
 		a = f.grep(reg[0]) {
 			|l|
 		
			r = l.match(reg[0])
		
			[r[1], r[2]]
		
 		}
 	
 		a.each {
 			|c, e|
 		
			nfnd = true
		
			areg = /#{reg[1] + c + reg[2]}/
		
 			f.grep(areg) {
 				|m|
	 		
				s = m.match(areg)
			
				puts " >>> " + e + " != " + s[1] if !(e.eql?(s[1]) || allow.include?(e + " <= " + s[1]))
				
				#print "." if !e.eql?(s[1]) && allow.include?(e + "=" + s[1])

				nfnd = false
			
				break
				
			}
			
			f.rewind if nfnd
		
 		}
 		
 	}

 }

require "find_longest_substring/version"

module FindLongestSubstring
	class Fls
		def self.find(input: nil, verbose: false, length: 2, resolve: false, printout: nil)

			return err "Invalid length" if length < 1

			if input.kind_of?(String) && File.exists?(input)
				printout = true if printout.nil?

				File.open(input) {
					|f|

					return self.new(f, verbose, length, resolve, printout)

				}

			end

			if input.nil?
				input = ARGF
				printout = true if printout.nil?

			end

			return self.new(input, verbose, length, resolve, printout)

		end

		def initialize(i, v, l, r, o)
			@v = v
			@s = ""

			if !r
				deb "init 1"

				err "Invalid input (1)" if !i.respond_to?('each_char')

				@s, t = lsub(i, l)

			else
				deb "init 2"

				res(i, l)

			end

			print @s if o

			return self

		end

		def lsub(i, l, f = false)
			t = []

			r, s = "", ""

			deb "Before: each_char"

			i.each_char {
				|c|

				deb "each_char: #{c}"

				if !t.include?(c)
					if t.length >= l
						return s, t if f

						r = s if s.length > r.length

						s, t = lsub(s.reverse, l - 1, true)

						s.reverse!

					end

					t << c

				end

				s += c

				deb s if !f

			}

			deb "After: each_char"

			return r, t

		end

		def res(i, l)
			s = (i.respond_to?('read') ? i.read : i)

			deb s

			err "Invalid input (2)" if !(s.respond_to?('scan') && s.respond_to?('slice'))

			a = []

			t = '[A-Za-z]' * l

			s.scan(/#{t}/) {
				|m|

				a << m

			}

			h = s.slice(0)

			h.scan(/#{t}/) {
				|m|

				a << m

			}

			a.uniq!

			b = []

			a.each {
				|p|

				b << p if a.include?(p.reverse) && p[0] > p[1]

			}

			a -= b

			a.each {
				|p|

				s.scan(/#{'[' + p + ']*'}/) {
					|m|

					@s = m if m.length > @s.length

				}

			}

		end

		def to_s
		@s

		end

		def to_str
		@s

		end

		def err(s)
			STDERR.puts " Error: " + s

			abort

		end

		def deb(f, *s)
			return if (!@v)

			STDERR.print " [DEBUG] " + f

			s.each {
				|o|

				w = " " * 3

				STDERR.print w + "::" + w + o.to_s

			}

			STDERR.puts

		end

	end

end

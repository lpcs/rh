# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'find_longest_substring/version'

Gem::Specification.new do |spec|
  spec.name          = "find_longest_substring"
  spec.version       = FindLongestSubstring::VERSION
  spec.authors       = ["Pavel Valena"]
  spec.email         = ["pavel.valena@sserver.cz"]

  spec.summary       = %q{Find longest substring of same n characters.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
end

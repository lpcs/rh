require 'spec_helper'

describe FindLongestSubstring do

	it "Test:1 - input:string - method:1" do
		i="abbdhajkdfbaaabbbabaaabbdfba"
		o="baaabbbabaaabb"

		expect(FindLongestSubstring::Fls.find(input: i).to_s).to eq(o)

	end

	it "Test:1 - input:string - method:2" do
		i="abbdhajkdfbaaabbbabaaabbdfba"
		o="baaabbbabaaabb"

		expect(FindLongestSubstring::Fls.find(input: i, resolve: true).to_s).to eq(o)

	end


	it "Test:1 - input:stdin - method:1 | not reading stdin" do
		i="abbdhajkdfbaaabbbabaaabbdfba"
		o="baaabbbabaaabb"


		expect(ARGF).to receive(:each_char).and_return(i)

		expect { FindLongestSubstring::Fls.find(verbose: true) }.to output(o).to_stdout

	end

	it "Test:1 - input:stdin - method:2" do
		i="abbdhajkdfbaaabbbabaaabbdfba"
		o="baaabbbabaaabb"

		expect(ARGF).to receive(:read).and_return(i)

		expect { FindLongestSubstring::Fls.find(resolve: true) }.to output(o).to_stdout

	end

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	it "Test:2 - input:string - method:1" do
		i="sdfsdddfsdffssdfsdsfsfsfsfsfsfsfsdfsdfdddd"
		o="sfsfsfsfsfsfsfs"

		expect(FindLongestSubstring::Fls.find(input: i).to_s).to eq(o)

	end

	it "Test:2 - input:string - method:2" do
		i="sdfsdddfsdffssdfsdsfsfsfsfsfsfsfsdfsdfdddd"
		o="sfsfsfsfsfsfsfs"

		expect(FindLongestSubstring::Fls.find(input: i, resolve: true).to_s).to eq(o)

	end

	it "Test:2 - input:stdin - method:2" do
		i="sdfsdddfsdffssdfsdsfsfsfsfsfsfsfsdfsdfdddd"
		o="sfsfsfsfsfsfsfs"

		expect(ARGF).to receive(:read).and_return(i)

		expect { FindLongestSubstring::Fls.find(resolve: true) }.to output(o).to_stdout

	end

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	it "Test:3 - input:string - method:1 - fail" do
		i="sdfsdddfsdffssdfsdsfsfsfsfsfsfsfsdfsdfdddd"
		o="sfsfsqqfsfsfsfs"

		expect(FindLongestSubstring::Fls.find(input: i).to_s).to eq(o)

	end

	it "Test:3 - input:string - method:2 - fail" do
		i="sdfsdddfsdffssdfsdsfsfsfsfsfsfsfsdfsdfdddd"
		o="sfsfsqqfsfsfsfs"

		expect(FindLongestSubstring::Fls.find(input: i, resolve: true).to_s).to eq(o)

	end

end

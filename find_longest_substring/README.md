
## Usage

require "find_longest_substring"

x = FindLongestSubstring::Fls.find(input: "Your string").to_s

FindLongestSubstring::Fls.find(input: "Your string", printout: true)


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).


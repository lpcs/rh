#!/usr/bin/env ruby
#
#	Author:	Pavel Valena
#	E-mail:	pavel.valena@sserver.cz
#
#	Description: Checks source code indentation between brackets { and }.
#		F.e. find's place where bracket is missing.
#
#	Usage:	check_indent.rb FILE1 [FILE2 [...]]
#
#

 class Syn
	@@tab = 4
	
 	def self.check(n, v = false)
		File.open(n) {
			|f|
			
			Syn.new(n, f, v)

		}
		
	end
	
	def initialize(n, f, v)
		@n = n
		@r = 0
		@v = v

		puts if v
		
		a = []
		
		d = 0
		
		x = 0
		y = 0
		
		k = false

		f.each_line {
			|l|
			
			@r += 1
			
			@c = 0

			if k
				if i = l.index(/\*\//m, @c)
					@c = i + 2
					k = false
				
				end

				next if k
				
			end
			
			l.gsub!(/\/+\*.*?\*\//m, '')

			h = l.index(/\/\//m, @c)
			
			if (i = l.index(/\/\*/m, @c)) && (!h || i < h)
				k = true				
				h = i
				
			end
			
			l = l[@c...h] if h

			l.gsub!(/"[^"]*"/m, '""')

			m = 0
			
			l[/\s*/m].each_char {
				|c|
				
				m += case c
					when "\t"; @@tab

					when " " ; 1

					when "\n"; 0

					else fail "Unknown space >" + c + "<"
				
				end
				
			}

			t = 0
				
			while i = l.index(/[{}]/m, @c)
				@c = i + 1

				if l[i] == "}"
					y += 1

					err "No bracket to close (left, right)", x, y if a.empty?
					
					e = a.pop
					
					while !e.eql?(m)
						err "Indent mismatch (got, expected, indents left, line)", m, e, a, l if d <= 0
		
						d -= 1
						e -= @@tab

						deb "<==", a
					
					end

					deb "<" + " " * 5, a

				else
					x += 1
					
					if t > 0
						a[a.size - 1] += @@tab
						d += 1

						deb "==>  ", a

					end

					g = i - 1

					g -= 1 while l[g] =~ /\s/m && g > 0
					
					m += @@tab if l[g] =~ /[,=]/m && d <= 0

					a << m + t

					t += @@tab * 2
						
					deb " " * 5 + ">", a
										
				end
				
			end
			
		}
		
		err "Brackets left open (cnt)", a.size, x, y if !(a.empty? && x.eql?(y))
 	
		deb "Total brackets", x

		true
		
 	end
 
 	def coords
		@n + ":" + (@r.to_s + ":" + @c.to_s).ljust(8)
	
	end
		
 	def err(*s)
		puts "\n ~~> Error in " + coords

		s.each {
			|o|
			
			puts "\t| " + o.to_s
		
		}
		
		puts
		
		abort

	end

 	def deb(*s)
		return if (!@v)

		print " [DEBUG] " + coords

		s.each {
			|o|
			
			w = " " * 3
			
			print w + "::" + w + o.to_s
		
		}
		
		puts
		
	end

 end
 
 puts
 
 ARGV.each() {
 	|a|

	print "Checking file '#{a}' ... "

	Syn.check(a)
	
	puts "ok"
	
 }

 puts "\n\n"
